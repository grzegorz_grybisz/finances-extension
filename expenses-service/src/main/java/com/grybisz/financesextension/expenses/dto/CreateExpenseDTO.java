package com.grybisz.financesextension.expenses.dto;

/**
 * Created by Grzesiek on 2016-11-21.
 */
public class CreateExpenseDTO {

    private String name;
    private double amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}