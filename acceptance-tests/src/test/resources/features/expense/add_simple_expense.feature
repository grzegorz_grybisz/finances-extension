Feature: Adding new simple expense
  In order to track my expenses
  As a user
  I want to be able to add new expenses to my account

  Scenario: Adding expenses
    Given I made the following expense
      | title | amount |
      | Bread | 3,5    |
      | Ticket| 2,80   |
    When I add this expense to my account
    Then I should be able to get it from my account