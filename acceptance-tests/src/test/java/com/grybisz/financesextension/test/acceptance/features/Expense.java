package com.grybisz.financesextension.test.acceptance.features;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Grzesiek on 2016-11-12.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features/expense")
public class Expense {
    //TODO change package name?
}
