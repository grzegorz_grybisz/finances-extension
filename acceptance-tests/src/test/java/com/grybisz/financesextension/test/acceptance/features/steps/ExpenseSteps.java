package com.grybisz.financesextension.test.acceptance.features.steps;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Created by Grzesiek on 2016-11-21.
 */
public class ExpenseSteps {


    @Given("I made the following expense")
    public void prepareExpenses(List<CreateExpenseDTO> arg1) throws Throwable {
        throw new PendingException();
    }

    @When("I add this expense to my account")
    public void addExpenseToAccount() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("I should be able to get it from my account")
    public void getExpenses() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
